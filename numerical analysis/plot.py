
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm

folderName="v(1,1)_(mu,nu)=(2,2)_sigmaA_SigmaB_[1.]_[1.]"
alpha_step=0.025
beta_step=0.025

#set range of alpha and beta as in the main code
rangeOf_alpha= np.arange(0.001, 1.001, alpha_step)
rangeOf_beta=np.arange(0.001,1.001, beta_step)


plt.rcParams.update({'font.size': 13})
#load data from files
currentA = np.loadtxt("data/"+folderName+"/currentA.txt", delimiter=' ', skiprows=0, dtype=float)
currentB = np.loadtxt("data/"+folderName+"/currentB.txt", delimiter=' ', skiprows=0, dtype=float)
densityA = np.loadtxt("data/"+folderName+"/densityA.txt", delimiter=' ', skiprows=0, dtype=float)
densityB = np.loadtxt("data/"+folderName+"/densityB.txt", delimiter=' ', skiprows=0, dtype=float)
junction = np.loadtxt("data/"+folderName+"/junction.txt", delimiter=' ', skiprows=0, dtype=float)

a=junction[:,3]
print("Jmax=",max(a))
pos=np.where(a==a.max())[0]
print("tilde(rho) corresponding to the maximal current: " ,junction[:,2][pos][0])
m=int(folderName[2])
n=int(folderName[4])
theta=max(a)/(min(m,n)*0.25)
print("theta=",theta*100)


phaseDiagram = np.loadtxt("data/"+folderName+"/phase_diagram.txt", delimiter=' ', skiprows=0, dtype=int)
phases=[x.split(' ')[1] for x in open("data/"+folderName+"/phase_indices").readlines()]
for i in range(len(phases)):
    phases[i]=phases[i].replace("\n","")
    phases[i]=phases[i].replace(",]","]")
print(phases)


n=currentA.shape[1]-2       #n is number of A
m=currentB.shape[1]-2       #m is number of b segments

#plot average current over all A segments over average densiyt over all A segments
    #prepare data
    ##calculate average current of A segments
JA=np.repeat(0,len(currentA[:,2]))
for i in range(n):
    JA=JA+currentA[:,2+i]
#np.divide(JA,np.repeat(n,len(JA)))
for i in range(len(JA)):
    JA[i]=JA[i]/n

if not os.path.exists("data/"+folderName+"/fig"):
    os.mkdir("data/"+folderName+"/fig")
if not os.path.exists("data/"+folderName+"/fig/eps"):
    os.mkdir("data/"+folderName+"/fig/eps")
if not os.path.exists("data/"+folderName+"/fig/jpg"):
    os.mkdir("data/"+folderName+"/fig/jpg")

#plot phase diagram
if not os.path.exists("data/tmp"):#a temporary folder where we create a file for each phase combination, and we save alpha,beta values inside of it
    os.mkdir("data/tmp")

filenames=[]
for x in range(len(phases)):
    phase=phases[x]
    fileOfPhase=open("data/tmp/"+phase+".txt","w")
    fileOfPhase.write("alpha beta\n-10 -10")    #header
    filenames.append(phase+".txt")
    for i in range( phaseDiagram.shape[0]):
        for j in range(phaseDiagram.shape[1]):
            if phaseDiagram[i][j]==x:
                fileOfPhase.write("\n"+str(rangeOf_alpha[j])+" "+str(rangeOf_beta[i]))
    fileOfPhase.close()
#now plot the Phase diagram using the prepared files in directory tmp
colors1 = cm.tab20c(np.linspace(0, 1, 10))
colors2 = cm.tab20b(np.linspace(0, 1, 10))

colors = cm.rainbow(np.linspace(0, 1, len(filenames)+6))
colors=["black","orange","red","forestgreen","tomato","sienna","olive","dimgray","brown","teal","steelblue","royalblue","darkslateblue","darkviolet","lightseagreen","palevioletred"]
ccc=[]
for i in range(0,10,1):
    ccc.append(colors1[i])
    ccc.append(colors2[9-i])
colors=ccc
colors=cm.tab20(np.linspace(0, 1, 20))
ccc=[]
for i in range(10):
    ccc.append(colors[i])
    ccc.append(colors[9+i])
colors=ccc

c1=cm.Dark2(np.linspace(0,1,8))
c2=cm.Set1(np.linspace(0,1,8))
c=cm.tab20(np.linspace(0, 1, 16))
ccc=[]
for i in range(8):
    ccc.append(c1[i])
    ccc.append(c2[7-i])
    ccc.append(c[2*i])



colors=ccc
markers=["x","8","+","o",".","*","v","^","p","H","X","D",">","<","P",",","1","2","3","4","s"]
plt.ylim(-0.1,1.1)
plt.xlim(-0.1,1.6)
plt.xlabel(r'$\alpha$')
plt.ylabel(r'$\beta$')
i=0
for file in filenames:
    data=np.loadtxt("data/tmp/"+file, delimiter=' ', skiprows=1, dtype=float)
    file=file.replace(".txt","")
    file=file.replace("A.","")
    file=file.replace("B.","")
    #file=file.replace(".LD","")
    #file=file.replace(".HD","")
    #file=file.replace(".MC","")
    #file=file.replace(".SP","")
    file=file.replace("*",":")
    plt.plot(data[:,1], data[:,0],marker=".",color=colors[i],label=file,linestyle = 'None')
    #plt.title("Phase Diagram: "+ folderName.replace("_"," "))
    if (i==2):i=i+1
    i=i+1
#plt.title("v(2:3), ("+r'$\mu,\nu$'+")=(1,1) ,$\sigma^{A}=[0.7,0.3]$, $\sigma^{B}=[0.5,0.35,0.15]$")
#plt.title("Phase Diagram: "+ folderName.replace("_"," "),fontsize=8)
plt.legend(fontsize=16)
plt.savefig("data/"+folderName+"/fig/jpg/phaseDiagramm"+ folderName.replace("_"," ")+".jpg",dpi=300)
plt.savefig("data/"+folderName+"/fig/eps/phaseDiagramm"+ folderName.replace("_"," ")+".eps",dpi=300)
plt.clf()







    ##calculate average current of B segments
JB=np.repeat(0,len(currentB[:,2]))
for i in range(m):
    JB=JB+currentB[:,2+i]
for i in range(len(JB)):
    JB[i]=JB[i]/m


    ##calculate average density of A segments
RA=np.repeat(0,len(densityA[:,2]))
for i in range(n):
    RA=RA+densityA[:,2+i]
for i in range(len(RA)):
    RA[i]=RA[i]/n


    ##calculate average density of B segments
RB=np.repeat(0,len(densityB[:,2]))
for i in range(m):
    RB=RB+densityB[:,2+i]
for i in range(len(RB)):
    RB[i]=RB[i]/m
#RB for example contains for each pair of alpha beta exctly one value, which is the average density of all B segments



##########     plot average current over average denisity for Asegments    ###############
plt.ylim(-0.001,0.3)
plt.xlim(-0.001,1.001)
plt.ylabel(r'$average \;J_A$')
plt.xlabel(r'$average\; \rho_A$')
plt.plot(RA,JA,"k.")
plt.savefig("data/"+folderName+"/fig/jpg/averagecurrentOverDensity_A.jpg",dpi=300)
plt.savefig("data/"+folderName+"/fig/eps/averagecurrentOverDensity_A.eps",dpi=300)
plt.clf()
##########    plot average current over average denisity for Bsegments   #################
plt.ylim(-0.001,0.3)
plt.xlim(-0.001,1.001)
plt.ylabel(r'$average \;J_B$')
plt.xlabel(r'$average\; \rho_B$')
plt.plot(RB,JB,"k.")
plt.savefig("data/"+folderName+"/fig/jpg/averagecurrentOverDensity_B.jpg",dpi=300)
plt.savefig("data/"+folderName+"/fig/eps/averagecurrentOverDensity_B.eps",dpi=300)
plt.clf()


######     for each segment plot current over average density    #############
for i in range(n):
    plt.ylim(-0.001,0.255)
    plt.xlim(-0.001,1.001)
    plt.ylabel(r'$J_A$'+str(i+1))
    plt.xlabel(r'$\rho_A$'+str(i+1))
    plt.plot(densityA[:,i+2],currentA[:,i+2],"k.")
    plt.savefig("data/"+folderName+"/fig/jpg/currentOverDensity_A"+str(i+1)+".jpg",dpi=300)
    plt.savefig("data/"+folderName+"/fig/eps/currentOverDensity_A"+str(i+1)+".eps",dpi=300)
    plt.clf()

for i in range(m):
    plt.ylim(-0.001,0.255)
    plt.xlim(-0.001,1.001)
    plt.ylabel(r'$J_B$'+str(i+1))
    plt.xlabel(r'$\rho_B$'+str(i+1))
    plt.plot(densityB[:,i+2],currentB[:,i+2],"k.")
    plt.savefig("data/"+folderName+"/fig/jpg/currentOverDensity_B"+str(i+1)+".jpg",dpi=300)
    plt.savefig("data/"+folderName+"/fig/eps/currentOverDensity_B"+str(i+1)+".eps",dpi=300)
    plt.clf()



#########       plot current of the junction over its density    ####################
plt.ylim(-0.001,0.301)
plt.xlim(-0.001,1.001)
plt.ylabel(r'$\tilde{J}$')
plt.xlabel(r'$\tilde{\rho}$')
plt.plot(junction[:,2],junction[:,3],"k.")
plt.savefig("data/"+folderName+"/fig/jpg/currentOverDensity_Junction.jpg",dpi=300)
plt.savefig("data/"+folderName+"/fig/eps/currentOverDensity_junction.eps",dpi=300)
plt.clf()


#plot heatmap of the model current
sc=plt.scatter(junction[:,0], junction[:,1], c=junction[:,3])
plt.colorbar(sc)
plt.ylabel(r'$\alpha$')
plt.xlabel(r'$\beta$')
plt.title("Model current")
plt.savefig("data/"+folderName+"/fig/jpg/ModelCurrent.jpg",dpi=300)
plt.savefig("data/"+folderName+"/fig/eps/ModelCurrent.eps",dpi=300)
plt.clf()
