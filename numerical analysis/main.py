# This Program serves to analyse TASEP dynamics at a multi-input-multi-output junction with complex kinetics.
# variable number of biased upstream and downstream segments as well as absorption and pumping rates at the junctionn site

import math
from itertools import product
from scipy.optimize import minimize
import os
import time
import numpy as np
import sys
begin = time.time()

# Define number of A segments:
m = 1
# Define number of B segments:
n = 1

# absorption rate
mu = 2
# Pumping rate
nu = 2

# define value rages of alpha and beta
alpha_step=0.025
beta_step=0.025
rangeOf_alpha = np.arange(0.001, 1.001, alpha_step)
rangeOf_beta = np.arange(0.001, 1.001, beta_step)
#rangeOf_alpha=[0,0.1,0.2,...]
#rangeOf_beta=[0,0.1,0.2,...]

#check correctness of the inserted parameters.
if m<=0 or n<=0:
    sys.exit("Error: invalid number of segments!")

if mu<0 or nu<0:
    sys.exit("mu and nu has to have positive values")

if alpha_step>=0.99 or beta_step>=0.99:
    sys.exit("alpha_setp and beta_setp should be lower 0.99")

# Define each Bias of the m A segments:
sigmaA = np.full(m, 1 / m)
#sigmaA=[0.25,0.75]         # set the values if the segments are biased
print("          SigmaA")
print("mean: ", np.mean(sigmaA))
print("var: ", np.var(sigmaA))
print("std: ", np.std(sigmaA))

if sum(sigmaA)!=1 or len(sigmaA)!=m:
    sys.exit("Length of sigmaA vector has to be m and its sum equal to 1")

# Define each bias of the n  B segments:
sigmaB = np.full(n, 1 / n)
#sigmaB=[0.1,0.3,0.6]    # set the values if the segments are biased
print("          SigmaB")
print("mean: ", np.mean(sigmaB))
print("var: ", np.var(sigmaB))
print("std: ", np.std(sigmaB))

if sum(sigmaB)!=1or len(sigmaB)!=n:
    sys.exit("Length of sigmaA vector has to be n and its sum equal to 1")



# create a folder inside data to save all the analysis results
folderName = "v(" + str(m) + "," + str(n) + ")_(mu,nu)=(" + str(mu) + "," + str(nu) + ")_sigmaA_SigmaB_" + str(
    sigmaA) + "_" + str(sigmaB)

if os.path.isdir('data'):
    print("directory 'data' exists")
else:
    os.mkdir("data")

if os.path.isdir('data/' + folderName):
    print("directory exists")
else:
    os.mkdir("data/" + folderName)
if n == m:
    nu = nu + 0.0001
    mu = mu - 0.000001


# funcion that checks if the matrix of a phase combination fulfills the minimal constraints to be accepted as potential phase combination
def checkCombi(oneMatrix):
    test: bool = False
    if np.array_equal(oneMatrix.sum(axis=0), np.ones(m + n, dtype=int)):  # if each segment has just one density phase
        # split matrix into 2: first for A segments , 2nd for B segments
        PhasesA = oneMatrix[:, :m]
        PhasesB = oneMatrix[:, m:m + n]

        # cooccurance of MC with LD or SP on A segments is not possible.
        sumOfRowsA = np.sum(PhasesA,
                            axis=1)  # sumOfRowsA is a vector containing the sum of each row of the phases matrix
        sumOfRowsB = np.sum(PhasesB, axis=1)  # the same is sumOfRowsB for B segments phases matrix
        if (sumOfRowsA[2] + sumOfRowsA[1] + sumOfRowsA[3] == sumOfRowsA[2]) | (sumOfRowsA[2] == 0):
            # Segments B: MC cant occur with HD or SP
            if (sumOfRowsB[2] + sumOfRowsB[0] + sumOfRowsB[3] == sumOfRowsB[2]) | (sumOfRowsB[2] == 0):
                test = True
    return test


# function getPhaseCombination translates a matrix of a phase combination to its notation as a string of the form A.LD.LD*B.HD.LD
def getPhaseCombination(OneCombination, m, n):
    PhasesA = OneCombination[:, :m]
    PhasesB = OneCombination[:, m:m + n]
    A_phase_names = "A"
    B_phase_names = "*B"
    for i in range(m):
        if (PhasesA[0][i] == 1):
            A_phase_names = A_phase_names + ".HD"
        elif (PhasesA[1][i] == 1):
            A_phase_names = A_phase_names + ".LD"
        elif (PhasesA[2][i] == 1):
            A_phase_names = A_phase_names + ".MC"
        elif (PhasesA[3][i] == 1):
            A_phase_names = A_phase_names + ".SP"

    for i in range(n):
        if (PhasesB[0][i] == 1):
            B_phase_names = B_phase_names + ".HD"
        elif (PhasesB[1][i] == 1):
            B_phase_names = B_phase_names + ".LD"
        elif (PhasesB[2][i] == 1):
            B_phase_names = B_phase_names + ".MC"
        elif (PhasesB[3][i] == 1):
            B_phase_names = B_phase_names + ".SP"
    return A_phase_names + B_phase_names


#             we define for each condition on entry, exit rates for A and B segments a function
#             that we can include in the constraints of the CO solver
# the return value of a function that belong to inequality constraint should have the form f(rho)>=0
# the return value of a function that belongs to an equality constraint should have the form f(rho) with f(rho)=0

epsilon = 0.00001  # we substitue epsilon from the inequality constraints as in the numerical analysis we have strict inequalities.


# Functions for the constraints on A segments
def Ai_HD1(x):  # first constraint taken from table: mu*sigmaA[i]*(1-rho)<1/2
    i = args[0]
    rho = x[0]
    return 0.5 - mu * sigmaA[i] * (1 - rho) - epsilon


def Ai_HD2(x):  # first constraint taken from table: mu*sigmaA[i]*(1-rho)<alpha
    i = args[0]
    rho = x[0]
    return alpha - mu * sigmaA[i] * (1 - rho) - epsilon


def Ai_LD(x):
    i = args[0]
    rho = x[0]
    return mu * sigmaA[i] * (1 - rho) - alpha - epsilon


def Ai_MC(x):
    i = args[0]
    rho = x[0]
    return mu * sigmaA[i] * (1 - rho) - 0.5 - epsilon


def Ai_SP1(x):
    i = args[0]
    rho = x[0]
    return mu * sigmaA[i] * (1 - rho) - alpha


def Ai_SP2(x):
    i = args[0]
    rho = x[0]
    return -mu * sigmaA[i] * (1 - rho) + alpha


###### Now functions for the constraints on B segments
def Bj_HD(x):
    j = args[1]
    rho = x[0]
    return nu * sigmaB[j] * rho - beta - epsilon


def Bj_LD1(x):
    j = args[1]
    rho = x[0]
    return 0.5 - nu * sigmaB[j] * rho - epsilon


def Bj_LD2(x):
    j = args[1]
    rho = x[0]
    return beta - nu * sigmaB[j] * rho - epsilon


def Bj_MC(x):
    j = args[1]
    rho = x[0]
    return nu * sigmaB[j] * rho - 0.5 - epsilon


def Bj_SP1(x):
    j = args[1]
    rho = x[0]
    return nu * sigmaB[j] * rho - beta


def Bj_SP2(x):
    j = args[1]
    rho = x[0]
    return -nu * sigmaB[j] * rho + beta


def bound1(x):
    rho = x[0]
    return rho - epsilon


def bound2(x):
    rho = x[0]
    return 1 - rho - epsilon


# we define the objective function of the CO solver
def objective(x):
    return x[0]  # x[0] is rhoTilde


args = [1, 2]


# function checkCO, tests the occurence of a phase combination by solving it as a constrained optimization problem
# with rho as unknown variable. if the constrained optimization solver succeed, the phase combi occurs for the given parameters
# we opt for COBYLA method which consists of solving the constrained optimisation by linear approximation.
# This is possible as all constraints have the form of a linear function of rhoTilde
def checkCO(oneCombi, m, n):
    constraintss = [{'type': 'ineq', 'fun': bound1},
                    {'type': 'ineq', 'fun': bound2}]  # an array to save all constraints
    # add constraint on A segments to the list of constraints
    # NB: in the constraints we have to input a function not a function call, otherwise we would
    # have the type of its return value and not the type function
    for i in range(m):
        args[0] = i
        if oneCombi[0][i] == 1:  # HD on A
            constraintss.extend([{'type': 'ineq', 'fun': Ai_HD1}, {'type': 'ineq', 'fun': Ai_HD2}])
        elif (oneCombi[1][i]) == 1:  # LD on A
            constraintss.append({'type': 'ineq', 'fun': Ai_LD})
        elif oneCombi[2][i] == 1:  # MC on A
            constraintss.append({'type': 'ineq', 'fun': Ai_MC})
        elif (oneCombi[3][i] == 1):  # SP , here we have an equality constraint so the type has to be set to eq
            constraintss.extend([{'type': 'ineq', 'fun': Ai_SP1}, {'type': 'ineq', 'fun': Ai_SP2}])

    # add constraint on B segments to the list of constraints
    for j in range(n):
        args[1] = j
        if oneCombi[0][m + j] == 1:  # HD
            constraintss.append({'type': 'ineq', 'fun': Bj_HD})
        elif oneCombi[1][m + j] == 1:  # LD
            constraintss.extend([{'type': 'ineq', 'fun': Bj_LD1}, {'type': 'ineq', 'fun': Bj_LD2}])
        elif oneCombi[2][m + j] == 1:  # MC
            constraintss.append({'type': 'ineq', 'fun': Bj_MC})
        elif oneCombi[3][m + j] == 1:  # SP, we have here an equality constraint
            constraintss.extend([{'type': 'ineq', 'fun': Bj_SP1}, {'type': 'ineq', 'fun': Bj_SP2}])

    # once we have the list of all constraints on entrance and exit rate
    # define bounds for unknown variable which is here rhoTilde
    x0 = [1.0, 1.0]  # initial value fo rho
    b = (0.0, 1.0)  # boundaries for rhotilde

    # run the solver and return the solution which contains the success state and the value of rhoTilde
    solution = minimize(objective, x0, method='COBYLA', constraints=constraintss)
    return solution


# function check: it checks for given entrance,exit,pumping,absorption,bias rates
# and for m and n, if the phase combination stored in matrix oneCombination can occur.
def check(oneCombination, m, n, mu, nu, alpha, beta, sigmaA, sigmaB):
    a = b = c = 0  # initialize a,b and c of the second degree equation of the current conservation
    PhasesA = oneCombination[:, :m]
    PhasesB = oneCombination[:, m:m + n]
    rho1 = 0
    rho2 = 0

    # check for each A segment if the conditions on alpha are satisfied
    # and if yes add the terms to a,b and c of the equation of the current conservation.
    for i in range(m):
        # HD: there is only one condition on alpha which depends on rhoTilde, so it has to be checked after solving the equation of the current conservation
        if (PhasesA[0][i] == 1):
            a = a - mu * mu * sigmaA[i] * sigmaA[i]
            b = b + 2 * mu * mu * sigmaA[i] * sigmaA[i] - mu * sigmaA[i]
            c = c + mu * sigmaA[i] - mu * mu * sigmaA[i] * sigmaA[i]
        # LD:alpha has to be less than 1/2
        elif (PhasesA[1][i] == 1) & (alpha < 0.5):
            c = c + alpha * (1 - alpha)
        # MC: alpha > 1/2 and current is equal to 1/4
        elif (PhasesA[2][i] == 1) & (alpha > 0.5):
            c = c + 0.25
        # SP
        elif (PhasesA[3][i] == 1) & (alpha < 0.5):
            a = a - mu * mu * sigmaA[i] * sigmaA[i]
            b = b + 2 * mu * mu * sigmaA[i] * sigmaA[i] - mu * sigmaA[i]
            c = c + mu * sigmaA[i] - mu * mu * sigmaA[i] * sigmaA[i]
        else:
            return False, 0  # phase combination can't occur for given alpha value

    for j in range(n):
        if (PhasesB[0][j] == 1) & (beta < 0.5):  # Bj has Phase HD
            c = c - beta * (1 - beta)
        elif PhasesB[1][j] == 1:  # LD
            a = a + nu * nu * sigmaB[j] * sigmaB[j]
            b = b - nu * sigmaB[j]
        elif (PhasesB[2][j] == 1) & (beta > 0.5):  # MC
            c = c - 0.25
        elif (PhasesB[3][j] == 1) & (beta < 0.5):  # SP
            a = a + nu * nu * sigmaB[j] * sigmaB[j]
            b = b - nu * sigmaB[j]
        else:
            return False, 0  # phase combination can not occur for given beta value

    # now we have the equation of the current conservation we solve it to get a value of rhoTilde
    testBothRho = False
    if (a == 0):
        if (b == 0):
            if c == 0:  # a=b=c=0 => the equation of current conservation does not give us any information about rhoTilde,
                solution = checkCO(oneCombination, m, n)  # so we solve it with CO solver
                if solution.success:
                    rho = objective(solution.x)  # problem solved with CO, rhoTilde=rho
                else:
                    return False, 0  # Phase combination can not occur
            else:
                return False, 0  # there is no current conservation if a=b=0 and c!=0
        else:  # if a=0 so  rhoTilde=-c/b
            rho = -c / b
    else:  # solve the second degree equation
        delta = b * b - 4 * a * c
        if (delta == 0):  # one solution
            rho = -b / (2 * a)
        elif delta > 0:  # two potential solution
            rho1 = (-b - math.sqrt(delta)) / (2 * a)
            rho2 = (-b + math.sqrt(delta)) / (2 * a)
            if ((rho1 >= 1) or (rho1 <= 0)) and ((rho2 >= 1) or (rho2 <= 0)):
                return False, 0  # rho1 and rho2 are out of range
            elif ((rho1 >= 1) or (rho1 <= 0)):
                rho = rho2  # only rho2 is in acceptable range
            elif ((rho2 >= 1) or (rho2 <= 0)):
                rho = rho1  # only rho1 is in acceptable range
            else:
                testBothRho = True  # both rho values are in acceptable range
        else:  # delta<0 , equation has no solution
            return False, 0

    # once the equation is solved we have a value for rhoTilde
    # and we can test the satisfiability of the remaining conditions on entrance and exit rates of the A segments
    for i in range(m):
        if (PhasesA[0][i] == 1):  # HD
            if testBothRho:
                if (mu * sigmaA[i] * (1 - rho1) >= 0.5) or (mu * sigmaA[i] * (1 - rho1) >= alpha):
                    testBothRho = False
                    rho = rho2
                if (mu * sigmaA[i] * (1 - rho2) >= 0.5) or (mu * sigmaA[i] * (1 - rho2) >= alpha):
                    testBothRho = False
                    rho = rho1
                if ((mu * sigmaA[i] * (1 - rho2) >= 0.5) or (mu * sigmaA[i] * (1 - rho2) >= alpha)) and (
                        (mu * sigmaA[i] * (1 - rho1) >= 0.5) or (mu * sigmaA[i] * (1 - rho1) >= alpha)):
                    return False, 0
            else:
                if (mu * sigmaA[i] * (1 - rho) >= 0.5) or (mu * sigmaA[i] * (1 - rho) >= alpha):
                    return False, 0  # HD cant occur for chosen parameters

        elif (PhasesA[1][i] == 1):  # LD
            if testBothRho:
                if (alpha >= mu * sigmaA[i] * (1 - rho1)):
                    testBothRho = False
                    rho = rho2
                if (alpha >= mu * sigmaA[i] * (1 - rho2)):
                    testBothRho = False
                    rho = rho1
                if (alpha >= mu * sigmaA[i] * (1 - rho2)) and (alpha >= mu * sigmaA[i] * (1 - rho1)):
                    return False, 0
            else:
                if (alpha >= mu * sigmaA[i] * (1 - rho)):
                    return False, 0  # LD can not occur for chosen parameters

        elif (PhasesA[2][i] == 1):  # MC
            if testBothRho:
                if (mu * sigmaA[i] * (1 - rho1) < 0.5):
                    testBothRho = False
                    rho = rho2
                if (mu * sigmaA[i] * (1 - rho2) < 0.5):
                    testBothRho = False
                    rho = rho1
                if (mu * sigmaA[i] * (1 - rho2) < 0.5) and (mu * sigmaA[i] * (1 - rho1) <= 0.5):
                    return False, 0
            else:
                if (mu * sigmaA[i] * (1 - rho) < 0.5):
                    return False, 0  # MC can not occur for chosen parameters

        elif (PhasesA[3][i] == 1):  # SP
            if testBothRho:
                if (alpha != mu * sigmaA[i] * (1 - rho1)):
                    testBothRho = False
                    rho = rho2
                if (alpha != mu * sigmaA[i] * (1 - rho2)):
                    testBothRho = False
                    rho = rho1
                if (alpha != mu * sigmaA[i] * (1 - rho1)) and (alpha != mu * sigmaA[i] * (1 - rho2)):
                    return False, 0
            else:
                if (alpha != mu * sigmaA[i] * (1 - rho)):
                    return False, 0  # SP can not occur for chosen parameters

    # test the satisfiability of the remaining conditions on entrance and exit rates of the B segments
    for j in range(n):
        if (PhasesB[0][j] == 1):  # HD
            if testBothRho:
                if (beta >= nu * sigmaB[j] * rho1) and (beta >= nu * sigmaB[j] * rho2):
                    return False, 0
                if (beta >= nu * sigmaB[j] * rho1):
                    testBothRho = False
                    rho = rho2
                if (beta >= nu * sigmaB[j] * rho2):
                    testRho2 = False
                    rho = rho1
            else:
                if (beta >= nu * sigmaB[j] * rho):
                    return False, 0  # HD can not occur on B segment for chosen parameters

        elif PhasesB[1][j] == 1:  # LD
            if testBothRho:
                if ((nu * sigmaB[j] * rho1 >= 0.5) or (nu * sigmaB[j] * rho1 >= beta)) and (
                        (nu * sigmaB[j] * rho2 >= 0.5) or (nu * sigmaB[j] * rho2 >= beta)):
                    return False, 0
                if (nu * sigmaB[j] * rho1 >= 0.5) or (nu * sigmaB[j] * rho1 >= beta):
                    testBothRho = False
                    rho = rho2
                if (nu * sigmaB[j] * rho2 >= 0.5) or (nu * sigmaB[j] * rho2 >= beta):
                    testBothRho = False
                    rho = rho1
            else:
                if (nu * sigmaB[j] * rho >= 0.5) or (nu * sigmaB[j] * rho >= beta):
                    return False, 0  # LD can not occur on B segment for chosen parameters

        elif PhasesB[2][j] == 1:  # MC
            if testBothRho:
                if (nu * sigmaB[j] * rho1 < 0.5) and (nu * sigmaB[j] * rho2 < 0.5):
                    return False, 0
                if nu * sigmaB[j] * rho1 < 0.5:
                    testBothRho = False
                    rho = rho2
                if nu * sigmaB[j] * rho2 < 0.5:
                    testBothRho = False
                    rho = rho1
            else:
                if nu * sigmaB[j] * rho < 0.5:
                    return False, 0  # MC can not occur on the B segment

        elif PhasesB[3][j] == 1:  # SP
            if testBothRho:
                if beta != nu * sigmaB[j] * rho1 and beta != nu * sigmaB[j] * rho2:
                    return False, 0
                if beta != nu * sigmaB[j] * rho1:
                    testBothRho = False
                    rho = rho2
                if beta != nu * sigmaB[j] * rho2:
                    testBothRho = False
                    rho = rho1
            else:
                if beta != nu * sigmaB[j] * rho:
                    return False, 0  # SP can not occur on the B segment

    # if all conditions are fulfilled, the phase combination occurs for the chosen parameters
    return True, rho


oneMatrice = True
allCombinations = []
possibleCombinations = []
# create matrices for phase combinations. if the matrix has more than 5 columns, there arise a memory bug
# in that case we devide the matrix into submatrices and work with
if (m + n) <= 5:
    #generate all binary matrices of size 4*(n+m)
    allCombinations = [[list(i[x:x + m + n]) for x in range(0, len(i), m + n)] for i in
                       product((0, 1), repeat=(m + n) * 4)]
    allCombinations = np.array(allCombinations)
    # filter out the matrices that contain phase combinations which occurance is impossible and save the result in possibleCombinations
    possibleCombinations = []
    for i in range(len(allCombinations)):
        if checkCombi(allCombinations[i]):
            possibleCombinations.append(allCombinations[i])
#all submatrices have maximal 5 columns
else:
    subMatriceLength = int((m + n) / 5)
    lastPieceLength = m + n - 4 * subMatriceLength
    submatrices = [[list(i[x:x + subMatriceLength]) for x in range(0, len(i), subMatriceLength)] for i in
                   product((0, 1), repeat=(subMatriceLength) * 4)]
    lastSubmatrices = [[list(i[x:x + lastPieceLength]) for x in range(0, len(i), lastPieceLength)] for i in
                       product((0, 1), repeat=(lastPieceLength) * 4)]
    submatrices = np.array(submatrices)
    lastSubmatrices = np.array(lastSubmatrices)
    sub = []
    for mm in submatrices:
        if (np.array_equal(mm.sum(axis=0),
                           np.ones(subMatriceLength, dtype=int))):  # if each segment has just one density phase
            sub.append(mm)
    submatrices = np.array(sub)
    sub = []
    for mm in lastSubmatrices:
        if (np.array_equal(mm.sum(axis=0),
                           np.ones(lastPieceLength, dtype=int))):  # if each segment has just one density phase
            sub.append(mm)
    lastSubmatrices = np.array(sub)

    oneMatrice = False

# write all phase combinations with their indices to a file: open the file
phaseIndex = open("data/" + folderName + "/phase_indices", "w")

# save the filenames of the stored data into filenames: open the file
filenames = open("data/" + folderName + "/files.txt", "w")

# matrix saves the index of a phase combination for the given
# alpha beta values, so that we have at the end a density phase diagramm
matrix = np.zeros((len(rangeOf_alpha), len(rangeOf_beta)), dtype=int)
 #a matrix containing the model current for given alpha beta values
ModelCurrent = np.zeros((len(rangeOf_alpha), len(rangeOf_beta)), dtype=float)
#the same for the current at the junction
current_at_junction = np.zeros((len(rangeOf_alpha), len(rangeOf_beta)), dtype=float)

currentA = []
currentB = []
densityA = []
densityB = []
junction = []

# get the phases that occur in the phase diagram
allphases = []  # a list in which we save the phase combinations that appear in the numerical resolution for the whole alpha beta ranges
j = -1
for beta in rangeOf_beta:
    j = j + 1
    l = -1
    for alpha in rangeOf_alpha:
        l = l + 1
        test_solved = False  #servs to check if for (alpha,beta) there is a matching phase combination
        numberOfmatchingPhases = 0
        phases = []     #in case of SP there is more than one phase combination that matches the current settings, store them in phases
        if oneMatrice: #n+m<5 => the matrices are defined
            for y in range(len(possibleCombinations)):  # for all phase combinations
                combi = possibleCombinations[y] #take one combination
                occurance, rhoTilde = check(combi, m, n, mu, nu, alpha, beta, sigmaA, sigmaB) #solve it with numerical analysis
                if occurance:
                    numberOfmatchingPhases += 1
                    phases.append(getPhaseCombination(combi, m, n))
                    test_solved = True
                    # once we know which density phase each segment has, we can store all data we need to plot later
                    phaseCombi = getPhaseCombination(combi, m, n)
                    combiA = combi[:, :m]   #phase matrix of A segments
                    combiB = combi[:, m:m + n]  #phase mtrix of B segments
                    curA = [alpha, beta] #curA is an array containing [alpha, beta, current(A1),current(A2)...current(Am)]
                    curB = [alpha, beta] #analoge to curA
                    densA = [alpha, beta] #densA is an array containing [alpha, beta, density(A1),density(A2)...density(Am)]
                    densB = [alpha, beta] # analoge to densA
                    junc = [alpha, beta, rhoTilde] #junc is an array containing [alpha,beta,densityAttheJunction]
                    current = 0
                    for i in range(m):
                        if combiA[0][i] == 1:  # Ai has phase HD=> density=1-exitRate , J=density*(1-density)
                            density = 1 - (mu * sigmaA[i] * (1 - rhoTilde))
                            densA.append(density)
                            curA.append(density * (1 - density))
                            current += density * (1 - density)
                        elif combiA[1][i] == 1:  # Ai has LD phase, density=alpha
                            densA.append(alpha)
                            curA.append(alpha * (1 - alpha))
                            current += alpha * (1 - alpha)
                        elif combiA[2][i] == 1:  # Ai has phase MC, density=0.5
                            densA.append(0.5)
                            curA.append(0.25)
                            current += 0.25
                        elif combiA[3][i] == 1:  # Ai has phase SP, average density=0.5, current=alpha*(1-alpha)
                            densA.append(0.5)
                            curA.append(alpha * (1 - alpha))
                            current += alpha * (1 - alpha)
                    junc.append(current)
                    current_at_junction[l][j] = current #save the value of the current at the junction
                    #save data of actual (alpha,beta), in order to obtain a dataset after checkig all alpha,beta values
                    junction.append(junc)
                    densityA.append(densA)
                    currentA.append(curA)

                    # now the same for B segments
                    for i in range(n):
                        if combiB[0][i] == 1:  # Bi has phase HD, density=(1-beta), current=density*(1-density)
                            densB.append(1 - beta)
                            curB.append(beta * (1 - beta))
                        elif combiB[1][i] == 1:  # Bi has phase LD, density=entrance rate
                            density = nu * sigmaB[i] * rhoTilde
                            densB.append(density)
                            curB.append(density * (1 - density))
                        elif combiB[2][i] == 1:  # Bi has phase MC
                            densB.append(0.5)
                            curB.append(0.25)
                        elif combiB[3][i] == 1:  # Bi has phase SP
                            densB.append(0.5)
                            curB.append(beta * (1 - beta))
                    densityB.append(densB)
                    currentB.append(curB)

        else: #n+m>5 => the matrices are built out of up to 5 submatrices
            # construct the phasecombi matrice from its subtiles:
            for m1 in submatrices:
                for m2 in submatrices:
                    for m3 in submatrices:
                        for m4 in submatrices:
                            for m5 in lastSubmatrices:
                                matrice = np.vstack((np.vstack((np.vstack(
                                    (np.vstack((m1.transpose(), m2.transpose())), m3.transpose())), m4.transpose())),
                                                     m5.transpose())).transpose()
                                #exactly the same steps as with the simple (not combined) matrix where n+m<=5
                                occurance, rhoTilde = check(matrice, m, n, mu, nu, alpha, beta, sigmaA, sigmaB)
                                if occurance:
                                    numberOfmatchingPhases += 1
                                    phases.append(getPhaseCombination(matrice, m, n))
                                    test_solved = True
                                    # once we know which density phase each segment has, we can store all data we need to plot later
                                    phaseCombi = getPhaseCombination(matrice, m, n)
                                    combiA = matrice[:, :m]
                                    combiB = matrice[:, m:m + n]
                                    curA = [alpha, beta]
                                    curB = [alpha, beta]
                                    densA = [alpha, beta]
                                    densB = [alpha, beta]
                                    junc = [alpha, beta, rhoTilde]
                                    current = 0
                                    for i in range(m):
                                        if combiA[0][
                                            i] == 1:  # Ai has phase HD=> density=1-exitRate , J=density*(1-density)
                                            density = 1 - (mu * sigmaA[i] * (1 - rhoTilde))
                                            densA.append(density)
                                            curA.append(density * (1 - density))
                                            current += density * (1 - density)
                                        elif combiA[1][i] == 1:  # Ai has LD phase, density=alpha
                                            densA.append(alpha)
                                            curA.append(alpha * (1 - alpha))
                                            current += alpha * (1 - alpha)
                                        elif combiA[2][i] == 1:  # Ai has phase MC, density=0.5
                                            densA.append(0.5)
                                            curA.append(0.25)
                                            current += 0.25
                                        elif combiA[3][
                                            i] == 1:  # Ai has phase SP, average density=0.5, current=alpha*(1-alpha)
                                            densA.append(0.5)
                                            curA.append(alpha * (1 - alpha))
                                            current += alpha * (1 - alpha)
                                    junc.append(current)
                                    junction.append(junc)
                                    current_at_junction[j][l] = current
                                    densityA.append(densA)
                                    currentA.append(curA)
                                    # now the same for B segments
                                    for i in range(n):
                                        if combiB[0][
                                            i] == 1:  # Bi has phase HD, density=(1-beta), current=density*(1-density)
                                            densB.append(1 - beta)
                                            curB.append(beta * (1 - beta))
                                        elif combiB[1][i] == 1:  # Bi has phase LD, density=entrance rate
                                            density = nu * sigmaB[i] * rhoTilde
                                            densB.append(density)
                                            curB.append(density * (1 - density))
                                        elif combiB[2][i] == 1:  # Bi has phase MC
                                            densB.append(0.5)
                                            curB.append(0.25)
                                        elif combiB[3][i] == 1:  # Bi has phase SP
                                            densB.append(0.5)
                                            curB.append(beta * (1 - beta))
                                    densityB.append(densB)
                                    currentB.append(curB)
        #check the number of matching phase combinations
        if numberOfmatchingPhases > 1:
            print("**************for (alpha,beta)=(", alpha, ",", beta,
                  ") there exist more then one phase combination:", phases)
            name = ""
            for phase in phases:
                if phase.count("MC") == m + n:
                    name = phase
            if name == "":
                name = "Shock_Phase"
        else:
            if test_solved is True:
                name = phases[0]
        if test_solved is True:
            if name not in allphases:
                allphases.append(name)  #save the name of the matching phase to all phase in order to build the density profile
                phaseIndex.write(str(allphases.index(name)) + " " + name + "\n") #save the phase and its index to a file
            ModelCurrent[l][j] = (phases[0].count("MC") / (m + n)) * current_at_junction[l][j]
            matrix[l][j] = allphases.index(name)
        else:
            print("No matching phase Combination for (alpha,beta)=", alpha, beta)
            matrix[j][l] = -1

#save data to files
filenames.write("data/" + folderName + "/phase_diagram.txt\n")
np.savetxt("data/" + folderName + "/phase_diagram.txt", matrix, fmt='%i')

filenames.write("data/" + folderName + "/ModelCurrent.txt\n")
np.savetxt("data/" + folderName + "/ModelCurrent.txt", current_at_junction, fmt='%1.5f')

filenames.write("data/" + folderName + "/currentA.txt\n")
np.savetxt("data/" + folderName + "/currentA.txt", currentA, fmt='%f')

filenames.write("data/" + folderName + "/currentB.txt\n")
np.savetxt("data/" + folderName + "/currentB.txt", currentB, fmt='%f')

filenames.write("data/" + folderName + "/densityA.txt\n")
np.savetxt("data/" + folderName + "/densityA.txt", densityA, fmt='%f')

filenames.write("data/" + folderName + "/densityB.txt\n")
np.savetxt("data/" + folderName + "/densityB.txt", densityB, fmt='%f')

filenames.write("data/" + folderName + "/junction.txt\n")
np.savetxt("data/" + folderName + "/junction.txt", junction, fmt='%f')

print("time= ", time.time() - begin)
