 

## Instructions for how to use the code for the generic numerical analysis of the transport dynamics at a complex MIMO TASEP junction


### Setup the model parameters
In the file main.py you have to set:
-**m** : the number of upstream segments (14)
-**n**: the number of downstream segments (16) 
-**mu**: the absorption rate (19)
-**nu**: the pumping rate  (21)
-**alpha_step** and **beta_step**: the model is analysed for alpha and beta values ranging between 0 and 1 with a step of **alpha_step** and **beta_step**. at first both are set to 0.025 such that we have each 40 alpha- and beta values. Increasing the number of alpha and beta values increases the precision of the phase diagram  (24,25)
-**sigmaA**: a vector of length m containing  the bias values of the m upstream segments. sum(sigmaA) has to be equal 1. (change line 43 if the segments are biased)
-**sigmaB**: a vector of length n containing  the bias values of the n downstream segments. sum(sigmaB) has to be equal 1. (change line 53 if the segments are biased)

### Running the algorithm
Now, to execute the algorithm simply enter the following command in the terminal: python3 main.py
Once the execution is terminated you find in data/ a directory with the name: v(<m>,<n>)_(mu,nu)=(<mu>,<nu>)_sigmaA_SigmaB_<sigmaA>_<sigmaB> containing the resulting data of the numerical analysis.
Inside the directory you find 9 files:
-files: containg the paths of the 8 remaining files.
-currentA.txt: alpha, beta, current at segment A1, current at segment A2,..., current at segment Am 
-currentB.txt: alpha, beta, current at segment B1, current at segment B2,..., current at segment Bn
-densityA.txt:  alpha, beta, density of segment A1, density of segment A2,...,density of segment Am
-densityB.txt:  alpha, beta, density of segment B1, density of segment B2,...,density of segment Bn
-junction.txt: alpha, beta, rhoTilde(density at the junction), Jtilde (current at the junction)
-ModelCurrent.txt: a matrix containg the model current for the different alpha beta values.
-phase_diagram.txt: a matrix containg the indices of the phases corresponding to the various alpha beta values.
-phase_indices.txt: gives the name of the density phase that corresponds to the index as to find in file "phase_diagram.txt"

### Plotting the results
To plot the obtained results set in file "plot.py":
-**folderName**: the name of the directory created within "data/", i.e. "v(<m>,<n>)_(mu,nu)=(<mu>,<nu>)_sigmaA_SigmaB_<sigmaA>_<sigmaB>" (line 7)
-**alpha_step** and **beta_step**: must have the same values as the alpha_step and beta_setp in main.py. (lines 8,9)
run the plotting python script by entering the following command in the terminal: python3 plot.py

You find the plots in data/v(<m>,<n>)_(mu,nu)=(<mu>,<nu>)_sigmaA_SigmaB_<sigmaA>_<sigmaB>/fig/jpg for *.jpg files and in data/v(<m>,<n>)_(mu,nu)=(<mu>,<nu>)_sigmaA_SigmaB_<sigmaA>_<sigmaB>/fig/eps for *.eps files.
plotted are: 
* The average current over density for A segments
* The average current over density for B segments
* Current over density of each segment seperately
* Crrent over density at the junction site
* A heatmap of the model current for the different alpha,beta values.
* Phase diagram of the studied model 
