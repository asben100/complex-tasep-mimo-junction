/* 
In this file, the simulation of a generalized TASEP model with complex kinetics at the junction.
Here is about the simulation of the totally asymmetric simple exclusion process with complex dynamics at the junction. 
It simualates the general model v(m,n) with aspiration rate (mu) and pumping rate (nu) at the junction.
Alpha and beta are the entrance and the exit rates of the whole model. 
Each of the m segments upstream the junction has its bias Sigma_Ai.
Each of the n segments downstream the junction has its bias Sigma_Bi.
All segments have the same number of sites (latticeSize).
The code simulates the model with the chosen parameters
It helps to collect resulting data such as average densities for each segment and the different current values.
*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <experimental/filesystem>
#include <filesystem>
#include <fstream>
#include <future>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <random>
#include <sstream>
#include <string>
#include <thread>
#include <vector>
#include<chrono>


//define some constants for this simulation
#define m 1                 //number of A segments
#define n 1                 //number of B segments
#define latticeSize 100      //length of each segment
#define mu 0.2                //absorption rate
#define nu 1                //pumping rate
#define gamma 1

//parameters for steady state time
#define cycle (m+n)*100		 //a snapshot of the system is taken each cyle to have density values without correlation effects
#define sampling_size 10000  //density values is samples over sampling_size values

//Density phase diagram contains the phase combinations of the segments for different alpha beta values in [0,1] with the following step
#define alpha_beta_step 0.025 


// #PS: change values in main function in case Segments have different bias values for entrance/exit of the junction
std::vector<double> Sigma_A(m,1.0/m);               //bias of A segments  
std::vector<double> Sigma_B(n,1.0/n);               //bias of B segments

//If you wish to track the transient time 
//set te time in which you want to start tracking the transient time and how many points you wonna have on the plot(its quality)
bool trackTransientTime=false; //set it to false if you dont wish to track the transient time
#define  t0 0
#define numberOfPlotPoints 100
//PS: change the value of t1 in OneSimulation function to set the time where you wonna stop tracking the transient time. t1>=t0+numberOfPlotPoints  

//Output file streams
std::ofstream currentFileA;
std::ofstream currentFileB;
std::ofstream averageDensityA;
std::ofstream averageDensityB;
std::ofstream first10A;
std::ofstream first10B;
std::ofstream last10A;
std::ofstream last10B;
std::ofstream densityProfileA;
std::ofstream densityProfileB;
std::ofstream junctionData;
std::ofstream fileNames;

// helper function to get filenames with nicely formated double values
std::string double_to_string(double value, int precision) {
    std::stringstream stream;
    stream << std::fixed << std::setprecision(precision) << value;
    return stream.str();
}

//get bias values well formulated to add it to the file names.
std::string get_bias(std::vector<double>bias){
    std::string s="[";
    for (double b:bias){
        s=s+double_to_string(b,2)+",";
    }
    s.pop_back();
    s=s+"]";
    return s;
}

//helper struct to give back the simulation result to the main function
struct ResultSet {
    std::vector<double> current_A;  //vector containing the current of each A segment
    std::vector<double> current_B;  //vector containing the current of each B segment
    std::vector<double> density_A;  //vector containing the average density of each A segment
    std::vector<double> density_B;  //vector containing the average density of each B segment

    //for density profiles of picked alpha beta values.
    std::vector<std::vector<double>> densities_A;   //matrice containing the average density for each site of A segments
    std::vector<std::vector<double>>  densities_B ; //matrice containing the average density for each site of A segments

    //junction data
    double rhoTilde;
    double JTilde;

    std::vector<std::vector<double>>densityOverTimeA;
    std::vector<std::vector<double>>densityOverTimeB;
    std::vector<int>snapshotStep;

};


//this function calculates the sum of a matrix
double sumOfDoubleMatrix(double mat[][latticeSize], int x){  
    double sum=0.0;
    int i, j; 
    for (i = 0; i < x; i=i+1) {
        for (j = 0; j < latticeSize; j=j+1) {
            sum += mat[i][j];
        }
    } 
    return sum;
}

//this function calculates the sum of a vector
double sumOfDoubleVector(std::vector<double> vect,int x){
    double sum=0.0;
    int i;
    for (i = 0; i < x; i=i+1) {
        sum=sum+vect[i];
    }
    return sum;
}

//this function runs a simualtion for one (alpha,beta) value pair
ResultSet One_Simulation(double alpha, double beta){
    //define the total time of simulation: total number of simulation steps
    long long transientTime=1000; //to define the 
    long long steps=transientTime*(m+n)*latticeSize + cycle*sampling_size;    //number of steps for each simulation: transient time+ staeady state time

    //to track transient time:
    long long t1=steps-1; //change t0 value if you wonna start tracking the transient time later on the simulation.
    std::vector<std::vector<double>>densityOverTimeA(m,std::vector<double>(numberOfPlotPoints,0.0));
    std::vector<std::vector<double>>densityOverTimeB(n,std::vector<double>(numberOfPlotPoints,0.0));
    std::vector<int>snapshotStep(numberOfPlotPoints,0);
    int snapshotDistance=(t1-t0)/numberOfPlotPoints;
    std::vector<std::vector<double>> densityA(m,std::vector<double>(latticeSize,0.0));
    std::vector<std::vector<double>> densityB(n,std::vector<double>(latticeSize,0.0));
    double totalTime=0;


        //model segments
    bool A_Segments[m][latticeSize];          //A segments
    bool B_Segments[n][latticeSize];          //B segments
    bool junction=false;

    //timepoint from which we are sure we have the steady state, so we can collect the data of the model 
    int steadyState=steps-cycle*sampling_size-1;

    //for gillespie algorith, we need random values between 0 and 1
    std::uniform_real_distribution<double> uniform_real_distibution{0.0, 1.0};
    std::default_random_engine generator;  // seed with time
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());

    //vectors and matrices to save the propensities of possible actions
    std::vector <double> enterToProp(n,alpha);
    double moveOnAProp[m][latticeSize];
    double moveOnBProp[n][latticeSize];
    std::vector <double> quitJunctionToProp(n,0.0);

    //for current calculation
    std::vector<double> Current_A(m,0.0);   //contains at the end of simulation the current of each segment. It will be wrapped in a struct and gived back to main function.
    std::vector<double> Current_B(n,0.0);

    //for average density
    std::vector<double> averageDensity_A(m,0.0);//contains at the end of simulation the average density of each segment. It will be wrapped in a struct and gived back to main function.
    std::vector<double> averageDensity_B(n,0.0);

    //for density profile
    std::vector<std::vector<double>> A_densities;   //a matrice containing at the end the average density per site
    std::vector<std::vector<double>> B_densities;

    //for data of the junction site
    double rhoTilde=0.0;
    double JTilde=0.0;


    //for density calculation
    std::vector<std::vector<double>> densityOnA(m,std::vector<double>(latticeSize,0.0));
    std::vector<std::vector<double>> densityOnB(n,std::vector<double>(latticeSize,0.0));
    double time_density=0.0;        //the total time of snapshots
    double time_steady_state=0.0;   //the total time in steady state. i.e the sum of tau values as defined for gillespie algorithm

    //initialization:
    for (int i=0;i<m;i=i+1){
        for (int j =0;j<latticeSize;j=j+1){
            A_Segments[i][j]=0;     //start the simulation with free segments
            moveOnAProp[i][j]=0.0;  //there is no particle on the segment, so no motion possibility 
        }
    }
    for (int i=0;i<n;i=i+1){
        for (int j =0;j<latticeSize;j=j+1){
            B_Segments[i][j]=0;
            moveOnBProp[i][j]=0.0;
        }
    }
    double r1=0.0;  //to compare the sum of propensities
    double tau=0.0; //the time at each simulation step. it has an exponential distribution


    int index=-1;
    //start simulation 
    for (int step=0;step<steps;step++){
        double sumOfA=sumOfDoubleMatrix(moveOnAProp,m);
        double sumOfB=sumOfDoubleMatrix(moveOnBProp,n);
        double sumOfEnter=sumOfDoubleVector(enterToProp,m);
        double sumOfQuit=sumOfDoubleVector(quitJunctionToProp,n);
        double sumOfPropensities=sumOfA+sumOfB+sumOfEnter+sumOfQuit; 


        //attribute random values to tau and r1
        std::exponential_distribution<double> exponential_distibution{sumOfPropensities};
        r1 = uniform_real_distibution(generator);
        tau = exponential_distibution(generator);
        totalTime+=tau;

        //to sum up propensities until reaching the target value= r1*sum of all propensities
        double running_sum = 0;
        double compareValue=r1*sumOfPropensities;  

        //find the right action based on the calculated propensities
            //action is to enter A segments
        if (compareValue<=sumOfEnter){
            for (int i=0;i<m;i++){
                running_sum=running_sum+enterToProp[i];
                if (running_sum>=compareValue){
                    A_Segments[i][0]=1;
                    //current: add 1 to  the number of movements along segment Ai if we are tracking the dynamics (in the steady state)
                    if (step>steadyState){
                        Current_A[i]+=1;
                        time_steady_state+=tau;
                    }
                    i=m;
                }
            }
        } 
            //action is to move within A segments
        else if (compareValue<=sumOfEnter+sumOfA){
            running_sum=sumOfEnter;
            for(int i=0;i<m;i++){
                for (int j=0;j<latticeSize;j++){
                    running_sum=running_sum+moveOnAProp[i][j];
                    if (running_sum>=compareValue){
                        A_Segments[i][j]=0;         //particle on segment i, site j is moving
                        if (j+1==latticeSize){ //particle quits to the junction site
                            junction=true;
                            if (step>steadyState){
                                JTilde+=1;
                                time_steady_state+=tau;
                            }
                        }else{
                            A_Segments[i][j+1]=true;
                            //current: add a new movement for segment Ai
                            if (step>steadyState){
                                Current_A[i]+=1;
                                time_steady_state+=tau;
                            }
                        }                  
                        i=m;
                        j=latticeSize;
                    }
                }
            }
        }
        
            //action is to quit the junction
        else if (compareValue<=sumOfEnter+sumOfA+sumOfQuit){
            running_sum=sumOfEnter+sumOfA;
            junction=false;         //particle leaves the junction
            for(int i=0;i<n;i++){
                running_sum=running_sum+quitJunctionToProp[i];
                if (running_sum>=compareValue){
                    B_Segments[i][0]=1;
                    //current of Segment Bi
                    if (step>steadyState){
                        Current_B[i]+=1;
                        time_steady_state+=tau;
                    }
                    i=n;
                }
            }
        }

            //action is on B segments
        else{
            running_sum=sumOfEnter+sumOfA+sumOfQuit;
            for(int i=0;i<n;i++){
                for (int j=0;j<latticeSize;j++){
                    running_sum=running_sum+moveOnBProp[i][j];
                    if (running_sum>=compareValue){
                        B_Segments[i][j]=0;
                        if (j+1<latticeSize){
                            B_Segments[i][j+1]=1;
                        }
                        if ((step>steadyState)&(j+1<latticeSize)){
                            Current_B[i]+=1;
                            time_steady_state+=tau;
                        }
                        i=n;
                        j=latticeSize;
                    }
                }
            }
        }

        //update possible actions and their propensities
            //A segments entrance propensities
        for (int i=0;i<m;i++){
            if (A_Segments[i][0]==false){
                enterToProp[i]=alpha;       //propensity to enter the model
            }else{
                enterToProp[i]=0;
            }
        }

            //propensities of moving particles along A segments, entering the junction is inclusive
        for(int i=0;i<m;i++){
            for (int j=0;j<latticeSize-1;j++){
                if ((A_Segments[i][j]==true)&(A_Segments[i][j+1]==false)){
                    moveOnAProp[i][j]=gamma;    //propensity to move forward on A segment
                }else{
                    moveOnAProp[i][j]=0;
                }
            }
            if ((junction==false)&(A_Segments[i][latticeSize-1]==true)){
                moveOnAProp[i][latticeSize-1]=Sigma_A[i]*mu;    //propensity to enter the junction
            }else{
                moveOnAProp[i][latticeSize-1]=0;
            }
        }

            //propensities for quitting the junction
        for (int i = 0; i < n; i++){
            if ((junction==true)&(B_Segments[i][0]==false)){
                quitJunctionToProp[i]=Sigma_B[i]*nu;
            }else{
                quitJunctionToProp[i]=0;
            }
        }

            //propensities to move along B segments. inclusive quitting the model
        for (int i=0;i<n;i++){
            for (int j=0;j<latticeSize-1;j++){
                if ((B_Segments[i][j]==true)&(B_Segments[i][j+1]==false)){
                    moveOnBProp[i][j]=gamma;    //propensity to move forward
                }else{
                    moveOnBProp[i][j]=0;
                }
            }
            if (B_Segments[i][latticeSize-1]==true){
                moveOnBProp[i][latticeSize-1]=beta; //propensity to quit the model
            }else{
                moveOnBProp[i][latticeSize-1]=0;
            }
        }


        //take density data from snapshots
        if ((step>=steadyState)&(step%cycle==0)){
            //density on A segments
            for (int i=0;i<m;i++){
                for (int j=0;j<latticeSize;j++){
                    if (A_Segments[i][j]){
                        densityOnA[i][j]+=tau;
                    }
                }
            }

            //density on B segments
            for (int i=0;i<n;i++){
                for (int j=0;j<latticeSize;j++){
                    if (B_Segments[i][j]){
                        densityOnB[i][j]+=tau;
                    }
                }
            }

            if(junction){
                rhoTilde+=tau;
            }

            //total time of snapshots
            time_density+=tau;
        }

        if (trackTransientTime==true){
            if((step%snapshotDistance==0)&(step>=t0)&(step<=t1)){
                index+=1;
                snapshotStep[index]=step;
            }
            for(int i=0;i<m;i++){
                for(int j=0;j<latticeSize;j++){
                    if (A_Segments[i][j])densityA[i][j]+=tau;
                }
                if((step%snapshotDistance==0)&(step>=t0)&(step<=t1)){
                    double avg=sumOfDoubleVector(densityA[i],latticeSize);
                    densityOverTimeA[i][index]=avg/(totalTime*latticeSize);
                }
            }


            for(int i=0;i<n;i++){
                for(int j=0;j<latticeSize;j++){
                    if (B_Segments[i][j])densityB[i][j]+=tau;
                }
                if((step%snapshotDistance==0)&(step>=t0)&(step<=t1)){
                    double avg=sumOfDoubleVector(densityB[i],latticeSize);
                    densityOverTimeB[i][index]=avg/(totalTime*latticeSize);
                }
            }
        }

    }//end of simulation steps


    //calculate average densities

        //average density on the junction
    rhoTilde=rhoTilde/time_density;
        
        //avrage density on A segments for each site
    for (int i=0;i<m;i++){
        double tmp=0.0;
        for (int j=0;j<latticeSize;j++){
            densityOnA[i][j]/=time_density;     //average density of each site
            tmp+=densityOnA[i][j];
        }
        averageDensity_A[i]=tmp/latticeSize;    //average density of the whole lattice
    }

        //avrage density on B segments
    for (int i=0;i<n;i++){
        double tmp=0.0;
        for (int j=0;j<latticeSize;j++){
            densityOnB[i][j]/=time_density;     //average density for each site
            tmp+=densityOnB[i][j];
        }
        averageDensity_B[i]=tmp/latticeSize;    //average density of the whole segment
    }


    //calculate current values
        //current at the junction
    JTilde=JTilde/time_steady_state;
        //calculate current for each segment Ai
    for (int i=0;i<m;i++){
        Current_A[i]=Current_A[i]/(latticeSize*time_steady_state);
    }
        //calculate current for each segment Bi
    for (int i=0;i<n;i++){
        Current_B[i]=Current_B[i]/(latticeSize*time_steady_state);
    }

    ResultSet resultset{Current_A,Current_B,averageDensity_A,averageDensity_B,densityOnA,densityOnB,rhoTilde,JTilde,densityOverTimeA,densityOverTimeB,snapshotStep};
    return resultset;

}



int main(){
    //TODO change bias values if needed
    //Sigma_B[0]= 0.9 ;
    //Sigma_B[1]= 0.1 ;
    //Sigma_A[0]=0.3;
    //Sigma_A[1]=0.2; 

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    std::string parameters="V("+std::to_string(m)+","+std::to_string(n)+")_(mu_nu)_(" + double_to_string(mu, 2) + "_" + double_to_string(nu, 2) +")_"+ get_bias(Sigma_A) +"_"+ get_bias(Sigma_B);
    //create folder with name containing v(n,m) bias and pumping absorption rate to save results within it    
    std::experimental::filesystem::create_directories("data/"+parameters+"/DensityProfiles");

    try{
        //create the vector for alpha beta vealues
        std::vector<double>range_alpha_beta;
        double i=0.001;
        while(i<1){
            range_alpha_beta.push_back(i);
            i=i+alpha_beta_step;
        }
        //you can set another range for alpha and/or beta values
        //range_alpha_beta={0.2,0.3,0.5,0.6,0.7,0.8,0.9,0.95};

        //define a matrix to store results for each alpha beta pair and run each simulation in a seperate thread
        std::vector<std::vector<ResultSet>> resultMatrix;
        for (double alpha:range_alpha_beta){
            std::cout<<"simulating the model for alpha="<<alpha<<"...\n";
            std::vector <std::future<ResultSet>> thread_tmp;
            std::vector<ResultSet> tmp;
            for (double beta:range_alpha_beta){
                thread_tmp.push_back(std::async(One_Simulation,alpha,beta));     
            }
            for (int i=0;i<range_alpha_beta.size();i++){
                tmp.push_back(thread_tmp.at(i).get());
            }
            resultMatrix.push_back(tmp);
        }

        //save results in files
        std::cout<<"saving results...\n";
        

            //open files and write within
        currentFileA.open("data/"+parameters+"/CurrentA.txt");
        currentFileB.open("data/"+parameters+"/CurrentB.txt");
        averageDensityA.open("data/"+parameters+"/DensityA.txt");
        first10A.open("data/"+parameters+"/first10A.txt");
        last10A.open("data/"+parameters+"/last10A.txt");
        first10B.open("data/"+parameters+"/first10B.txt");
        last10B.open("data/"+parameters+"/last10B.txt");
        averageDensityB.open("data/"+parameters+"/DensityB.txt");
        junctionData.open("data/"+parameters+"/Junction.txt");
        fileNames.open("data/"+parameters+"/fileNames.txt");
        fileNames<<"CurrentA.txt\n"<<"CurrentB.txt\n"<<"DensityA.txt\n"<<"DensityB.txt\n"<<"Junction.txt\n"<<"first10A.txt\n"<<"first10B.txt\n"<<"last10A.txt\n"<<"last10B.txt\n";


        //header of current and average density files as well as the junction data file
        currentFileA<<"alpha beta";
        currentFileB<<"alpha beta";
        averageDensityA<<"alpha beta";
        first10A<<"alpha beta";
        first10B<<"alpha beta";
        last10A<<"alpha beta";
        last10B<<"alpha beta";
        averageDensityB<<"alpha beta";
        junctionData<<"alpha beta rhoTilde JTilde";
        for(int i=0;i<m;i++){
            currentFileA<<" JA"<<i+1;
            averageDensityA<<" rhoA"<<i+1;
            first10A<<"first10A"<<i+1;
            last10A<<"last10A"<<i+1;
        }
        for(int i=0;i<n;i++){
            currentFileB<<" JB"<<i+1;
            averageDensityB<<" rhoB"<<i+1;
            first10B<<"first10B"<<i+1;
            last10B<<"last10B"<<i+1;
        }


        for (int i=0;i<range_alpha_beta.size();i++){
            for(int j=0;j<range_alpha_beta.size();j++){
                ResultSet rs=resultMatrix.at(i).at(j);
                currentFileA<<"\n"<<range_alpha_beta[i]<<" "<<range_alpha_beta[j];
                currentFileB<<"\n"<<range_alpha_beta[i]<<" "<<range_alpha_beta[j];
                averageDensityA<<"\n"<<range_alpha_beta[i]<<" "<<range_alpha_beta[j];
                first10A<<"\n"<<range_alpha_beta[i]<<" "<<range_alpha_beta[j];
                last10A<<"\n"<<range_alpha_beta[i]<<" "<<range_alpha_beta[j];
                first10B<<"\n"<<range_alpha_beta[i]<<" "<<range_alpha_beta[j];
                last10B<<"\n"<<range_alpha_beta[i]<<" "<<range_alpha_beta[j];
                averageDensityB<<"\n"<<range_alpha_beta[i]<<" "<<range_alpha_beta[j];
                junctionData<<"\n"<<range_alpha_beta[i]<<" "<<range_alpha_beta[j]<<" "<<rs.rhoTilde<<" "<<rs.JTilde;
                for (int l=0;l<m;l++){
                    currentFileA<<" "<<rs.current_A[l];
                    averageDensityA<<" "<<rs.density_A[l];
                    double first10=0.0;
                    double last10=0.0;
                    for (int ll=5;ll<15;ll++){
                        first10=first10+rs.densities_A[l][ll];   
                    }
                    for (int ll=85;ll<95;ll++){
                        last10=last10+rs.densities_A[l][ll];   
                    }
                    first10A<<" "<<first10/10;
                    last10A<<" "<<last10/10;

                }
                for (int l=0;l<n;l++){
                    currentFileB<<" "<<rs.current_B[l];
                    averageDensityB<<" "<<rs.density_B[l];
                    double first10=0.0;
                    double last10=0.0;
                    for (int ll=5;ll<15;ll++){
                        first10=first10+rs.densities_B[l][ll];   
                    }
                    for (int ll=85;ll<95;ll++){
                        last10=last10+rs.densities_B[l][ll];   
                    }
                    first10B<<" "<<first10/10;
                    last10B<<" "<<last10/10;
                }
            }
        }



        //SET:for picked alpha beta indices, save the data for density profiles
        std::vector<int>alphaIndiciesForDensityProfile{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38}; //we have * values of alpha, we pick * out of them
        std::vector<int>betaIndiciesForDensityProfile{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38};//the same for beta values
        std::cout<<"saving data for density profiles for picked alpha beta values...\n";
        std::ofstream profiles;
        profiles.open("data/"+parameters+"/DensityProfiles/filenames_density_profile.txt");
        //save data to plot density profiles of each segment along the lattice
        std::string filename;
        for( int indexi:alphaIndiciesForDensityProfile){
            for(int  indexj:betaIndiciesForDensityProfile){
                //a file for the Density profiles of A segments
                std::ofstream DensityProfileA;
                filename = "ProfileA_(alpha,beta)_("+ double_to_string(range_alpha_beta[indexi],3)+","+ double_to_string(range_alpha_beta[indexj],3)+parameters+  ".txt";
                DensityProfileA.open("data/"+parameters+"/DensityProfiles/"+filename);
                profiles<<filename<<"\n";

                //a file for the Density profiles of B segments
                std::ofstream DensityProfileB;
                filename = "ProfileB_(alpha,beta)_("+ double_to_string(range_alpha_beta[indexi],3)+","+ double_to_string(range_alpha_beta[indexj],3)+parameters+  ".txt";
                DensityProfileB.open("data/"+parameters+"/DensityProfiles/"+filename);
                profiles<<filename<<"\n";

                //header of the files
                DensityProfileA<<"alpha 1-beta site"; 
                DensityProfileB<<"alpha 1-beta site";
                for(int l=0;l<m;l++){DensityProfileA<<" RhoA"<<l;}
                for(int l=0;l<n;l++){DensityProfileB<<" RhoB"<<l;}

                for(int ll=0;ll<latticeSize;ll++){
                    DensityProfileA<<"\n"<<range_alpha_beta[indexi]<<" "<<1-range_alpha_beta[indexj]<<" "<<ll;
                    DensityProfileB<<"\n"<<range_alpha_beta[indexi]<<" "<<1-range_alpha_beta[indexj]<<" "<<ll;
                    for(int l=0;l<m;l++){DensityProfileA<<" "<<resultMatrix.at(indexi).at(indexj).densities_A[l][ll];}
                    for(int l=0;l<n;l++){DensityProfileB<<" "<<resultMatrix.at(indexi).at(indexj).densities_B[l][ll];}
                }
            }
        }


        //SET: for picked alpha beta values, track transient time for different segments
        //for that we change the result set so that it gives back two matrices containing 
        if(trackTransientTime){
            std::cout<<"save data to track transient time...\n";
            //write save data from resultset.densityOverTimeA/B and snapshotStep
            std::vector <int> alphaIndiciesForTrackingTransient {3,5,7,11,15,19,23,28,32,38};
            std::vector <int> betaIndiciesForTrackingTransient {3,5,7,11,15,19,23,28,32,38};
            std::experimental::filesystem::create_directories("data/"+parameters+"/trackTransientTime");
            
            //alphaIndiciesForTrackingTransient=alphaIndiciesForDensityProfile;
            //betaIndiciesForTrackingTransient=betaIndiciesForDensityProfile;
            std::ofstream filenames;
            filenames.open("data/"+parameters+"/trackTransientTime/filenames.txt");
            for( int indexi:alphaIndiciesForTrackingTransient){
                for(int  indexj:betaIndiciesForTrackingTransient){
                    ResultSet rs=resultMatrix.at(indexi).at(indexj);
                    std::ofstream fileA;
                    std::ofstream fileB;
                    fileA.open("data/"+parameters+"/trackTransientTime/TransientA_v("+std::to_string(m)+","+std::to_string(n)+")_(alpha,beta)_("+double_to_string(range_alpha_beta[indexi],3)+","+ double_to_string(range_alpha_beta[indexj],3)+").txt");
                    fileB.open("data/"+parameters+"/trackTransientTime/TransientB_v("+std::to_string(m)+","+std::to_string(n)+")_(alpha,beta)_("+double_to_string(range_alpha_beta[indexi],3)+","+ double_to_string(range_alpha_beta[indexj],3)+").txt");
                    filenames<<"TransientA_v("+std::to_string(m)+","+std::to_string(n)+")_(alpha,beta)_("+double_to_string(range_alpha_beta[indexi],3)+","+ double_to_string(range_alpha_beta[indexj],3)+").txt\n";
                    filenames<<"TransientB_v("+std::to_string(m)+","+std::to_string(n)+")_(alpha,beta)_("+double_to_string(range_alpha_beta[indexi],3)+","+ double_to_string(range_alpha_beta[indexj],3)+").txt\n";
                    //write header of files:
                    fileA<<"step";
                    fileB<<"step";
                    for (int i=0;i<m;i++)fileA<<" rhoA"<<i;
                    for (int i=0;i<n;i++)fileB<<" rhoB"<<i;
                    for (int i=0;i<rs.snapshotStep.size();i++){
                        fileA<<"\n"<<rs.snapshotStep[i];
                        fileB<<"\n"<<rs.snapshotStep[i];
                        for(int j=0;j<m;j++)fileA<<" "<<rs.densityOverTimeA[j][i];
                        for(int j=0;j<n;j++)fileB<<" "<<rs.densityOverTimeB[j][i];
                    }
                }
            }
        }
       


    }catch (const std::exception& e) {
        std::cout << e.what();
    }

    long long transientTime=10000; //to define the 
    long long steps=transientTime*(m+n)*latticeSize+cycle*sampling_size; 
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout <<"Steps="<<steps<< "\nTime = " << std::chrono::duration_cast<std::chrono::seconds> (end - begin).count() << "[s]" << std::endl;
    return 0;

}
