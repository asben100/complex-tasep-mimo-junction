 
## Instructions for how to use the algorithm to simulate the transport dynamics at a complex MIMO TASEP junction


### Setup the model parameters
In the file main.cpp you have to set:
-**m** : the number of upstream segments (33)
-**n**: the number of downstream segments (34) 
-** latticeSize**: the lattice size i.e. the number of sites of the various segments (35)
-**mu**: the absorption rate (36)
-**nu**: the pumping rate  (37)
-**alpha_beta_step**: The model is simulated for alpha and beta values ranging between 0 and 1 with a step of **alpha_beta_step** . At first, it is set to 0.025 such that we have each 40 alpha- and beta values. Increasing the number of alpha and beta values increases the precision of the phase diagram  (45)
-**trackTransientTime**: If you wish to track the transient time of the system turn the value into True (54)
-**Sigma_A[i], Sigma_B[j]**: to be changed if the segments are biased (472)

Values which can be changed, would aber influence the efficiency and the precision of the resulting data.
-**gamma**: jumping rate always set to 1 (38).
-**cycle**: a snapshot of the system is taken each cyle to have density values without correlation effects (41)
-**sampling_size**: each cycle a snapshot of the model is taken on which basis the data is taken (density at the sites, segments, and junction) to avoid correlation effects. sampling_size is the number of snapshots for which we take the average values. (42)
-**transientTime**: change this values if you wish to start tracking data earlier/later in the simulation. actually set to 10^4. (138)

### Running the algorithm
Now, to execute the algorithm simply enter the following commands in the terminal: 

g++ -std=c++11 main.cpp -o main -lstdc++fs -pthread
./main

nce the execution is terminated you find in data/ a directory with the name:
 v(<m>,<n>)_(mu,nu)=(<mu>,<nu>)_<sigmaA>_<sigmaB> containing the resulting data of the numerical analysis.
Inside the directory you find 9 files:
-fileNames: containg the paths of the remaining files.
-currentA.txt: alpha, beta, current at segment A1, current at segment A2,..., current at segment Am 
-currentB.txt: alpha, beta, current at segment B1, current at segment B2,..., current at segment Bn
-densityA.txt:  alpha, beta, density of segment A1, density of segment A2,...,density of segment Am
-densityB.txt:  alpha, beta, density of segment B1, density of segment B2,...,density of segment Bn
-junction.txt: alpha, beta, rhoTilde(density at the junction), Jtilde (current at the junction)
-first/last10A/B: containing the average density of first/last 10 sites of each A/B segment.
You find also one directory named DensityProfiles containing the required data to plot the density profiles of the various segments.
If during the simulation **trackTransientTime** is set to True you'll find a further directory named TrackTransient containing the required data to track the transient time by plotting the evolution of the density value over the time.

### Plotting the results
To plot the obtained results set in file "plotData.py":
-**folderName**: the name of the directory created within "data/", i.e. "v(<m>,<n>)_(mu,nu)=(<mu>,<nu>)_<sigmaA>_<sigmaB>" (line 10)

-**trackTransient**: if you have data to track the transient time, set the value to True. (line 13)

run the plotting python script by entering the following command in the terminal: python3 plotData.py

You find the plots in data/v(<m>,<n>)_(mu,nu)=(<mu>,<nu>)_<sigmaA>_<sigmaB>/fig/jpg for *.jpg files and in data/v(<m>,<n>)_(mu,nu)=(<mu>,<nu>)_<sigmaA>_<sigmaB>/fig/eps for *.eps files.
plotted are: 
* The average current over density for A segments
* The average current over density for B segments
* Current over density of each segment seperately
* Crrent over density at the junction site
* Phase diagram of the studied model 

The plots for density profiles of the model segments are in directory: data/v(<m>,<n>)_(mu,nu)=(<mu>,<nu>)_<sigmaA>_<sigmaB>/fig/densityProfiles/- jpg or eps. 
The plotted average density of the segments over the time to track the transient time is to find in directory: data/v(<m>,<n>)_(mu,nu)=(<mu>,<nu>)_<sigmaA>_<sigmaB>/fig/TrackTransient/- jps or eps
