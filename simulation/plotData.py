#In this file, we read the data produced by the simulations, define the density phase diagrams based on it,
#and plot the phase diagram, the density profiles as well as the current values
import sys

import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm

epsilon=0.03

folderName="xy"

#there is data to track the transient time?
trackTransient=False


with open("data/"+folderName+"/fileNames.txt") as filenames:
    lines = filenames.readlines()
    for filename in lines:
        filename=filename.replace("\n","")
        if "CurrentA" in filename:
            #read current data of A segments
            currentA = np.loadtxt("data/"+folderName+"/"+filename, delimiter=' ', skiprows=1, dtype=float)
        elif "CurrentB" in filename:
            print("currentB")
            currentB = np.loadtxt("data/"+folderName+"/"+filename, delimiter=' ', skiprows=1, dtype=float)
        elif "DensityA" in filename:
            print("dnsityA")
            densityA = np.loadtxt("data/"+folderName+"/"+filename, delimiter=' ', skiprows=1, dtype=float)
        elif "DensityB" in filename:
            densityB=np.loadtxt("data/"+folderName+"/"+filename, delimiter=' ', skiprows=1, dtype=float)
        elif "first10A" in filename:
            first10A=np.loadtxt("data/"+folderName+"/"+filename, delimiter=' ', skiprows=1, dtype=float)
        elif "first10B" in filename:
            first10B=np.loadtxt("data/"+folderName+"/"+filename, delimiter=' ', skiprows=1, dtype=float)
        elif "last10A" in filename:
            last10A=np.loadtxt("data/"+folderName+"/"+filename, delimiter=' ', skiprows=1, dtype=float)
        elif "last10B" in filename:
            last10B=np.loadtxt("data/"+folderName+"/"+filename, delimiter=' ', skiprows=1, dtype=float)
        else:
            print("junction")
            junction=np.loadtxt("data/"+folderName+"/"+filename, delimiter=' ', skiprows=1, dtype=float)

#difference between the average density at the beginning and at the end of the lattice
devA=last10A[:,2:]-first10A[:,2:]
devB=last10B[:,2:]-last10B[:,2:]


m=first10A.shape[1]-2       #n is number of A
n= first10B.shape[1] - 2       #m is number of b segments
allPhases=[]

f = open("data/"+folderName+"/DensityPhase.txt","w")
for l in range(first10A.shape[0]):
    phases=""
    for i in range(m):
        if(densityA[l][2+i]<0.48)&(devA[l][i]<epsilon):
            phases=phases+"LD"
        elif (densityA[l][2+i]>0.52)&(devA[l][i]<epsilon):
            phases=phases+"HD"
        elif (densityA[l][2+i]<0.52)&(densityA[l][2+i]>0.48)&(devA[l][i]<epsilon):
            phases=phases+"MC"
        else:
            phases=phases+"SP"
    phases=phases+":"
    for i in range(n):
        if(densityB[l][2+i]<0.48)&(devB[l][i]<epsilon):
            phases=phases+"LD"
        elif (densityB[l][2+i]>0.52)&(devB[l][i]<epsilon):
            phases=phases+"HD"
        elif (densityB[l][2+i]<0.52)&(densityB[l][2+i]>0.48)&(devB[l][i]<epsilon):
            phases=phases+"MC"
        else:
            phases=phases+"SP"
    #define and write the phase combination detected
    f.write(str(first10A[l][0])+" "+str(first10A[l][1])+" "+phases+"\n")
    if (phases not in allPhases):
        allPhases.append(phases)
f.close()


#load alpha beta values from previous file
if not os.path.exists("data/"+folderName+"/tmp"):
    os.mkdir("data/"+folderName+"/tmp")
with open("data/"+folderName+"/DensityPhase.txt") as f:
    lines = f.readlines()
filenames=[]
for phase in allPhases:
    fileOfPhase=open("data/"+folderName+"/tmp/"+phase+".txt","w")
    fileOfPhase.write("alpha beta\n0 0")
    filenames.append(phase+".txt")
    for line in lines:
        if (phase in line):
            fileOfPhase.write("\n"+line.split()[0]+" "+line.split()[1])
    fileOfPhase.close()

# create folders to save the plots within
if not os.path.exists("data/"+folderName+"/fig"):
    os.mkdir("data/"+folderName+"/fig")
if not os.path.exists("data/"+folderName+"/fig/eps"):
    os.mkdir("data/"+folderName+"/fig/eps")
if not os.path.exists("data/"+folderName+"/fig/jpg"):
    os.mkdir("data/"+folderName+"/fig/jpg")

#plot phase diagram
colors = cm.rainbow(np.linspace(0, 1, len(filenames)))
markers=[".","v","^","8","p","*","H","+","X","D",">","<","x","P","o",",","1","2","3","4","s"]
plt.ylim(-0.1,1.1)
plt.xlim(-0.1,1.8)
plt.xlabel(r'$\alpha$')
plt.ylabel(r'$\beta$')
i=0
for file in filenames:
    data=np.loadtxt("data/"+folderName+"/tmp/"+file, delimiter=' ', skiprows=1, dtype=float)
    file=file.replace(".txt","")
    plt.plot(data[:,0], data[:,1],marker=markers[i],label=file,linestyle = 'None')
    i=i+1
plt.legend()
plt.savefig("data/"+folderName+"/fig/jpg/phaseDiagramm.jpg")
plt.savefig("data/"+folderName+"/fig/eps/phaseDiagramm.eps")
plt.clf()      

#for each segment plot current over average density
for i in range(m):
    #plt.ylim(-0.001,0.255)
    plt.xlim(-0.001,1.001)
    plt.ylabel(r'$J_A$'+str(i+1))
    plt.xlabel(r'$\rho_A$'+str(i+1))
    plt.plot(densityA[:,i+2],currentA[:,i+2],"k.")
    plt.savefig("data/"+folderName+"/fig/jpg/currentOverDensity_A"+str(i+1)+".jpg")
    plt.savefig("data/"+folderName+"/fig/eps/currentOverDensity_A"+str(i+1)+".eps")
    plt.clf()

for i in range(n):
    #plt.ylim(-0.001,0.255)
    plt.xlim(-0.001,1.001)
    plt.ylabel(r'$J_B$'+str(i+1))
    plt.xlabel(r'$\rho_B$'+str(i+1))
    plt.plot(densityB[:,i+2],currentB[:,i+2],"k.")
    plt.savefig("data/"+folderName+"/fig/jpg/currentOverDensity_B"+str(i+1)+".jpg")
    plt.savefig("data/"+folderName+"/fig/eps/currentOverDensity_B"+str(i+1)+".eps")
    plt.clf()


#plot current of the junction over its density
#plt.ylim(-0.001,0.5)
plt.xlim(-0.001,1.001)
plt.ylabel(r'$\tilde{J}$'+str(i+1))
plt.xlabel(r'$\tilde{\rho}$'+str(i+1))
plt.plot(junction[:,2],junction[:,3],"k.")
plt.savefig("data/"+folderName+"/fig/jpg/currentOverDensity_Junction.jpg")
plt.savefig("data/"+folderName+"/fig/eps/currentOverDensity_junction.eps")
plt.clf()



cc=["k","orange","c","m","y","b"]
#plot density profiles
if not os.path.exists("data/"+folderName+"/fig/DensityProfiles/eps"):
    os.mkdir("data/"+folderName+"/fig/DensityProfiles")
    os.mkdir("data/"+folderName+"/fig/DensityProfiles/eps")
if not os.path.exists("data/"+folderName+"/fig/DensityProfiles/jpg"):
    os.mkdir("data/"+folderName+"/fig/DensityProfiles/jpg")
#read data for density profiles:
with open("data/"+folderName+"/DensityProfiles/filenames_density_profile.txt") as filenames:
    lines = filenames.readlines()
    for filename in lines:
        filename=filename.replace("\n","")
        print(filename)
        data=np.loadtxt("data/"+folderName+"/DensityProfiles/"+filename, delimiter=' ', skiprows=1, dtype=float)
        figname=filename[0:34]+")"
        if ("ProfileA" in figname):
            plt.ylim(-0.001,1.001)
            plt.xlim(-0.001,105)
            plt.ylabel(r'$\rho_Ai$')
            plt.xlabel("Lattice")
            for i in range(m):
                plt.plot(data[:,2],data[:,i+3],label=r'$\rho_A$'+str(i+1),color=cc[i])
            plt.plot(data[:,2],data[:,0],label=r'$\alpha$',color="r")
            plt.plot(data[:,2],data[:,1],label=r'$1-\beta$',color="b")
            plt.legend()
            plt.savefig("data/"+folderName+"/fig/DensityProfiles/jpg/"+figname+".jpg")
            plt.savefig("data/"+folderName+"/fig/DensityProfiles/eps/"+figname+".eps")
            plt.clf()
        elif ("ProfileB" in figname):
            print(data[0])
            plt.ylim(-0.001,1.001)
            plt.xlim(-0.001,105)
            plt.ylabel(r'$\rho_Bi$')
            plt.xlabel("Lattice")
            for i in range(n):
                print(data)
                plt.plot(data[:,2],data[:,i+3],label=r'$\rho_B$'+str(i+1),color=cc[i])
                print(data[0])
            plt.plot(data[:,2],data[:,0],label=r'$\alpha$',color="r")
            plt.plot(data[:,2],data[:,1],label=r'$1-\beta$',color="b")
            plt.legend()
            plt.savefig("data/"+folderName+"/fig/DensityProfiles/jpg/"+figname+".jpg")
            plt.savefig("data/"+folderName+"/fig/DensityProfiles/eps/"+figname+".eps")
            plt.clf()


#plot data to track the transient time
if not os.path.exists("data/"+folderName+"/fig/TrackTransient/eps"):
    os.mkdir("data/"+folderName+"/fig/TrackTransient")
    os.mkdir("data/"+folderName+"/fig/TrackTransient/eps")
if not os.path.exists("data/"+folderName+"/fig/TrackTransient/jpg"):
    os.mkdir("data/"+folderName+"/fig/TrackTransient/jpg")

if not trackTransient:
    sys.exit()
with open("data/"+folderName+"/trackTransientTime/filenames.txt") as filenames:
    lines = filenames.readlines()
    for filename in lines:
        filename=filename.replace("\n","")
        data=np.loadtxt("data/"+folderName+"/trackTransientTime/"+filename, delimiter=' ', skiprows=1, dtype=float)
        figname=filename.replace(".txt","")
        if("TransientA" in figname):
            plt.ylabel(r'$\rho_Ai$')
            plt.xlabel("Time")
            plt.ylim(-0.001,1.001)
            for i in range(m):
                plt.plot(data[:,0],data[:,i+1],label=r'$\rho_A$'+str(i+1),color=cc[i])
            plt.legend()
            plt.savefig("data/"+folderName+"/fig/TrackTransient/jpg/"+figname+".jpg")
            plt.savefig("data/"+folderName+"/fig/TrackTransient/eps/"+figname+".eps")
            plt.clf()
        elif("TransientB" in figname):
            plt.ylabel(r'$\rho_Bi$')
            plt.xlabel("Time")
            plt.ylim(-0.001,1.001)
            for i in range(n):
                plt.plot(data[:,0],data[:,i+1],label=r'$\rho_B$'+str(i+1),color=cc[i])
            plt.legend()
            plt.savefig("data/"+folderName+"/fig/TrackTransient/jpg/"+figname+".jpg")
            plt.savefig("data/"+folderName+"/fig/TrackTransient/eps/"+figname+".eps")
            plt.clf()


