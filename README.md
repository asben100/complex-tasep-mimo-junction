<link>

# Transport at a complex multiple-input-multiple-output (MIMO) TASEP junction

***IMPORTANT: Please read the NOTICE and LICENSE files in this repository for important copyright and licensing information regarding the contents of this project.***

Within this repository, you can find the code of the generic numerical analysis a MIMO TASEP junction with complex dynamics as well as the code for its stochastic simulation. 

## Content

The repository is split into two folders, each of which are summarised below.

### Folder "Numerical_analysis"

This folder contains our algorithm for analytical resolution of the complex TASEP model (written in python 3). It also contains a script for plotting the obtained results, written in Python 3. A further README for setting and running all of these codes is provided within the folder.


### Folder "Simulation_code"

This folder contains the code for the stochastic simulation using the Gillespie alorithm, written in C++. It also contains a script for plotting the obtained results, written in Python 3. A further README for setting and running all of these codes is provided within the folder.



